public class VentanaRectangular {
    private String  nombre;
    private Rectangulo cuadro;
    public VentanaRectangular(Rectangulo rectangulo, String name) {
        this.cuadro = rectangulo;
        this.nombre = name;
    }
    public Rectangulo getCuadro() {
        return cuadro;
    }
    public void setCuadro(Rectangulo cuadro) {
        this.cuadro = cuadro;
    }
}
