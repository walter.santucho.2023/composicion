public class VentanaCircular
{
    private String  nombre;
    private Circulo circulo;
    public VentanaCircular(Circulo circ, String name)
    {
        this.circulo = circ;
        this.nombre = name;
    }
    public Circulo getCirculo() {
        return circulo;
    }

    public void setCirculo(Circulo circulo) {
        this.circulo = circulo;
    }
}