import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;

public class Testing
{
    @Test
    public void testAreaRectangulo()
    {
        Rectangulo rectangulo = new Rectangulo(3,4);
        VentanaRectangular vr = new VentanaRectangular(rectangulo, "Word");
        Double resultado;
        resultado = vr.getCuadro().calcularArea();
        assertEquals(12.0, resultado, 0.1);
    }

    @Test
    public void testAreaCirculo()
    {
        Circulo circulo = new Circulo(5);
        VentanaCircular vc = new VentanaCircular(circulo, "Foxit");
        Double resultado;
        resultado = vc.getCirculo().calcularArea();
        assertEquals(78.5, resultado, 0.1);
    }

}
