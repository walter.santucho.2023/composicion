public class Rectangulo
{
    private double ancho;
    private double alto;

    public Rectangulo(double ancho, double alto)
    {
        this.ancho = ancho;
        this.alto = alto;
    }
    public double calcularArea()
    {
        return ancho * alto;
    }

    public String toString()
    {
        return "Rectángulo (Ancho: " + ancho + ", Alto: " + alto + ")";
    }
}




