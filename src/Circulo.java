public class Circulo
{
    private double radio;

    public Circulo(double radio) {
        this.radio = radio;
    }

    public double calcularArea() {
        return Math.PI * radio * radio;
    }

    public String toString() {
        return "Círculo (Radio: " + radio + ")";
    }
}
